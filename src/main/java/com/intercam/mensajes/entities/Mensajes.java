package com.intercam.mensajes.entities;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Mensajes implements Serializable {

    private Integer id;
    private String contenido;
}


